Test demonstrating a segfault when trying to save a class that uses a custom allocator using ROOT's I/O.
The contained `run.sh` script should be all that's needed, it gets ROOT using the LCG views.
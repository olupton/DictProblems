#!/bin/bash
set -e
. /cvmfs/sft.cern.ch/lcg/views/LCG_96b/x86_64-centos7-gcc9-dbg/setup.sh
# . /cvmfs/sft-nightlies.cern.ch/lcg/views/dev3/Tue/x86_64-centos7-gcc9-opt/setup.sh
CC=g++
root-config --version
genreflex -s selection.xml test.h -o test_dict.cpp
${CC} `root-config --cflags --ldflags --libs` -ggdb -o test test_dict.cpp test.cpp
valgrind -q --track-origins=yes --num-callers=30 --suppressions=${ROOTSYS}/etc/valgrind-root.supp ./test
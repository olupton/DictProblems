#include "test.h"

#include <TFile.h>
#include <TTree.h>

#include <cassert>
#include <iostream>
#include <memory>

int main() {
  static constexpr auto fname   = "test.root";
  static constexpr auto tname   = "TestTree";
  static constexpr auto name_os = "test_obj_std";
  static constexpr auto name_oe = "test_obj_exp";
  static constexpr auto name_od = "test_obj_def";
  static constexpr auto name_ps = "test_ptr_std";
  static constexpr auto name_pe = "test_ptr_exp";
  static constexpr auto name_pd = "test_pts_def";
  unsigned int          value   = 42;
  constexpr bool        crash{true};
  auto                  resource = reinterpret_cast<MemoryResource*>( 0x12345678 );
  {
    // Create an output file containing various different objects
    std::unique_ptr<TFile> ofile{TFile::Open( fname, "recreate" )};
    ofile->cd();
    auto otree = new TTree( tname, "" );

    MyStruct<>               obj_std; // ~vector<int>
    MyStruct<custom_alloc<>> obj_def,
        obj_exp{resource};                 // ~vector<int, custom_alloc<int>> with {default, explicit} allocator state
    MyStructPtrs<>               ptrs_std; // ~vector<struct>
    MyStructPtrs<custom_alloc<>> ptrs_def,
        ptrs_exp{resource}; // ~vector<struct, custom_alloc<struct>> with {default, explicit} allocator state
    assert( obj_exp.get_allocator().resource() == resource );
    assert( ptrs_exp.get_allocator().resource() == resource );
    assert( obj_def.get_allocator().resource() == defaultResource() );
    assert( ptrs_def.get_allocator().resource() == defaultResource() );
    obj_std.push_back( value );
    obj_def.push_back( value );
    obj_exp.push_back( value );
    ptrs_std.emplace_back( &value, 1 );
    if ( crash ) {
      ptrs_def.emplace_back( &value, 1 );
      ptrs_exp.emplace_back( &value, 1 );
    }
    otree->Branch( name_os, &obj_std );
    otree->Branch( name_oe, &obj_exp );
    otree->Branch( name_od, &obj_def );
    otree->Branch( name_ps, &ptrs_std );
    otree->Branch( name_pe, &ptrs_exp );
    otree->Branch( name_pd, &ptrs_def );
    otree->Fill();
    otree->Write();
    ofile->Close();
  }
  std::unique_ptr<TFile> ifile{TFile::Open( fname, "read" )};
  assert( ifile );
  TTree* itree{nullptr};
  ifile->GetObject( tname, itree );
  assert( itree );
  MyStruct<>*                  obj_std{nullptr};
  MyStruct<custom_alloc<>>*    obj_def{nullptr}, *obj_exp{nullptr};
  MyStructPtrs<>*              ptrs_std{nullptr};
  MyStructPtrs<custom_alloc<>>*ptrs_def{nullptr}, *ptrs_exp{nullptr};
  itree->SetBranchAddress( name_os, &obj_std );
  itree->SetBranchAddress( name_oe, &obj_exp );
  itree->SetBranchAddress( name_od, &obj_def );
  itree->SetBranchAddress( name_ps, &ptrs_std );
  itree->SetBranchAddress( name_pe, &ptrs_exp );
  itree->SetBranchAddress( name_pd, &ptrs_def );
  itree->GetEntry( 0 );
  assert( obj_std && obj_exp && obj_def && ptrs_std && ptrs_exp && ptrs_def );
  assert( obj_std->size() == 1 );
  assert( obj_exp->size() == 1 );
  assert( obj_def->size() == 1 );
  assert( ptrs_std->size() == 1 );
  assert( ptrs_exp->size() == crash );
  assert( ptrs_def->size() == crash );
  // Check the values in the simple struct
  assert( obj_std->front() == value );
  assert( obj_exp->front() == value );
  assert( obj_def->front() == value );
  // Check the values in the more complex struct
  assert( ptrs_std->front().size() == 1 );
  assert( *ptrs_std->front().data() == value );
  if ( crash ) {
    assert( ptrs_exp->front().size() == 1 );
    assert( ptrs_def->front().size() == 1 );
    assert( *ptrs_exp->front().data() == value );
    assert( *ptrs_def->front().data() == value );
  }
  // Check how the allocators were set up
  assert( obj_def->get_allocator().resource() == defaultResource() );
  assert( ptrs_def->get_allocator().resource() == defaultResource() );

  // Apparently ROOT knows that std::vector allocators should not be persisted
  assert( obj_exp->get_allocator().resource() == defaultResource() );
  assert( ptrs_exp->get_allocator().resource() == defaultResource() );

  return 0;
}
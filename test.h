#ifndef TEST_H
#define TEST_H
#include <boost/compressed_pair.hpp>

#include <cassert>
#include <vector>

using MemoryResource = void;

[[nodiscard]] inline MemoryResource* defaultResource() { return reinterpret_cast<MemoryResource*>( 0xdeadbeef ); }

/** Stateful allocator. This is just a dummy that doesn't actually make use of its state.
 */
template <typename T = void>
struct custom_alloc {
  using value_type                             = T;
  using propagate_on_container_swap            = std::true_type;
  using propagate_on_container_copy_assignment = std::true_type;
  using propagate_on_container_move_assignment = std::true_type;

  constexpr custom_alloc( MemoryResource* resource = defaultResource() ) noexcept : m_resource{resource} {}

  template <typename U>
  constexpr custom_alloc( custom_alloc<U> const& other ) noexcept : m_resource{other.m_resource} {}

  T* allocate( std::size_t n ) { return std::allocator<T>{}.allocate( n ); }

  void deallocate( T* p, std::size_t n ) noexcept { std::allocator<T>{}.deallocate( p, n ); }

  template <typename U>
  friend constexpr bool operator==( custom_alloc const& lhs, custom_alloc<U> const& rhs ) {
    return lhs.m_resource == rhs.m_resource;
  }

  [[nodiscard]] MemoryResource* resource() const noexcept { return m_resource; }

private:
  template <typename>
  friend struct custom_alloc;

  MemoryResource* m_resource{nullptr};
};

template <typename T, typename U>
inline constexpr bool operator!=( custom_alloc<T> const& lhs, custom_alloc<U> const& rhs ) {
  return !( lhs == rhs );
}

/** Simple wrapper for vector<unsigned int, allocator<unsigned int>>
 */
template <typename Alloc = std::allocator<void>>
struct MyStruct final {
  using data_t         = unsigned int;
  using allocator_type = typename std::allocator_traits<Alloc>::template rebind_alloc<data_t>;
  using data_vec_t     = std::vector<data_t, allocator_type>;
  MyStruct( allocator_type alloc = {} ) : m_data{alloc} {}
  [[nodiscard]] decltype( auto ) size() const { return m_data.size(); }
  [[nodiscard]] decltype( auto ) front() const { return m_data.front(); }
  [[nodiscard]] allocator_type   get_allocator() const { return m_data.get_allocator(); }
  void                           push_back( data_t data ) { m_data.push_back( data ); }

private:
  data_vec_t m_data;
};

/** Wrapper for vector<data_t, allocator<data_t>> where data_t is a non-trivial struct.
 */
template <typename Alloc = std::allocator<void>>
struct MyStructPtrs final {
  struct data_t final {
    unsigned int        m_len{0};
    unsigned int const* m_buf{nullptr}; //[m_len]
    data_t() = default;
    data_t( unsigned int const* buf, unsigned int len ) : m_len{len}, m_buf{buf} {}
    [[nodiscard]] unsigned int        size() const { return m_len; }
    [[nodiscard]] unsigned int const* data() const { return m_buf; }
  };
  using allocator_type = typename std::allocator_traits<Alloc>::template rebind_alloc<data_t>;
  using data_vec_t     = typename std::vector<data_t, allocator_type>;
  MyStructPtrs( allocator_type alloc = {} ) : m_data{alloc} {}
  [[nodiscard]] allocator_type   get_allocator() const { return m_data.get_allocator(); }
  [[nodiscard]] decltype( auto ) size() const { return m_data.size(); }
  [[nodiscard]] decltype( auto ) front() const { return m_data.front(); }
  void emplace_back( unsigned int const* buf, unsigned int len ) { m_data.emplace_back( buf, len ); }

private:
  data_vec_t m_data;
};
#endif